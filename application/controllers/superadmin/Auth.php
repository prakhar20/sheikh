<?php 
class Auth extends CI_Controller
{
	/*
	Login Page
	 */
	public function index()
	{
		if($this->session->userdata('isLoggedIn') == true)
		{
			/*
			Auto Redirect to the admin welcome page
			 */
			redirect('superadmin/dashboard');		
		}
		else
		{
			//show login page
			$this->load->view('superadmin/auth/login');
		}
	}
	/*
	Handle Login Action 
	default Password "  leader@324!#"
	 */
	public function login()
	{
		$this->load->library('bcrypt');
		$this->load->model('auth_model','auth');
		/*echo $this->bcrypt->hash_password("leader@324!#");*/
		$userInput =  $this->input->post();
		
		$verificationStatus = $this->auth->authenticate($userInput['email'],$userInput['password']);
		if(count($verificationStatus))
		{
			$this->session->set_userdata('userInfo',$verificationStatus);
			$this->session->set_userdata('isLoggedIn',true);
			redirect($this->agent->referrer());
		}

		else
		{
			redirect($this->agent->referrer());
		}
		

	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url('superadmin/auth'));
	}
}