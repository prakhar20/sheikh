<?php 
class Events extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('events_model','events');
	}

	public function index()
	{

		$collection['collection'] = $this->events->getCollection();
		$this->load->view('superadmin/html/head',array('additional'=>array('css_external'=>array(
			'https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css',
			))));			
		$this->load->view('superadmin/html/header');		
		$this->load->view('superadmin/html/nav');			
		$this->load->view('superadmin/events/grid',$collection);			
		$this->load->view('superadmin/html/footer',array('additional'=>array('js_external'=>array(
							'https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js',
							'https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js',
							'https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js',
							'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js',
							'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js',
							'https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js',
							'https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js '	
													))));	
	}

	public function add()
	{
		$this->load->view('superadmin/html/head');			
		$this->load->view('superadmin/html/header');		
		$this->load->view('superadmin/html/nav');			
		$this->load->view('superadmin/events/create');			
		$this->load->view('superadmin/html/footer');
	}
	public function calender()
	{


		foreach($this->events->getCollection()->result() as $event)
		{
			$response[] =  array(
							'title'=> $event->event_title,
							'start'=> $event->date,
							'imageurl'=> base_url($event->image),
						);

		}

		$this->load->view('superadmin/html/head');		
		$this->load->view('superadmin/html/header');		
		$this->load->view('superadmin/html/nav');			
		$this->load->view('superadmin/calendar',array('collection'=>$response));			
		$this->load->view('superadmin/html/footer');;			
		// $this->load->view('superadmin/html/footer',array('additional'=>array('js_internal'=>array('assets/js/calender_custom.js'))));	
	}

	public function save()
	{
		$postData  = $this->input->post();
		
			//validation is true
			
		   		$config['upload_path']          = './assets/uploads/';
                $config['allowed_types']        = 'gif|jpg|png';

                if(!is_dir($config['upload_path']))
                mkdir($config['upload_path'],777,true);

                $this->load->library('upload', $config);

				// var_dump($_FILES['image']['size']);die;                

                if($_FILES['image']['size'])
                {
				             
	                if(!$this->upload->do_upload('image')  )
	                {

											
						$this->session->set_flashdata('notice_details',$this->upload->display_errors());
						$this->session->set_flashdata('notice_type_icon','ban');
						$this->session->set_flashdata('notice_type','danger');
						$this->session->set_flashdata('notice_text','Error');
	                
	                }
	                else
	                {
						$uploadData = array('upload_data' => $this->upload->data());
	                }	
                	
                }
 				if(isset($uploadData))
                $postData['image'] = $config['upload_path'].$uploadData['upload_data']['file_name'];
                // $this->load->model('events_model','events');

	                if($this->events->create($postData))
	                {
	                $this->session->set_flashdata('notice_details','');
					$this->session->set_flashdata('notice_type','success');
					$this->session->set_flashdata('notice_type_icon','check');
					$this->session->set_flashdata('notice_text','Saved..!');
                		if(isset($postData['id']))
                		redirect($this->agent->referrer(),'refresh');
                		else
                		redirect(base_url('superadmin/events'),'refresh');
	                	return;
	                }
	                else
	                {
	                $this->session->set_flashdata('notice_details','Some Error Occured');
					$this->session->set_flashdata('notice_type_icon','ban');
					$this->session->set_flashdata('notice_type','danger');
					$this->session->set_flashdata('notice_text','Error');

	                }

                redirect(base_url('superadmin/events'),'refresh');
                return;

                
	}

	public function edit($param)
	{

		// var_dump(is_numeric($param));
		if(is_numeric($param))
		{
 

		$this->db->where('id',$param);
		$event['collection'] = $this->events->getCollection();
		$event['edit'] = true;
		$this->load->view('superadmin/html/head');			
		$this->load->view('superadmin/html/header');		
		$this->load->view('superadmin/html/nav');			
		$this->load->view('superadmin/events/create',$event);			
		$this->load->view('superadmin/html/footer');	
		}
	}

	public function delete($param)
	{
		if(is_numeric($param))
		{
		$this->db->where('id',$param);
		$this->db->delete('events');
					$this->session->set_flashdata('notice_details','Deleted The Event');
					$this->session->set_flashdata('notice_type_icon','ban');
					$this->session->set_flashdata('notice_type','danger');
					$this->session->set_flashdata('notice_text','Deleted');
        redirect(base_url('superadmin/events'),'refresh');
		}
	}


	public function rsvp()
	{
		$response = $this->db->get('rsvp_response');
		$this->load->view('superadmin/html/head',array('additional'=>array('css_external'=>array(
			'https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css',
			))));			
		$this->load->view('superadmin/html/header');		
		$this->load->view('superadmin/html/nav');			
		$this->load->view('superadmin/events/rsvp',array('collection'=>$response));			

		$this->load->view('superadmin/html/footer',array('additional'=>array('js_external'=>array(
							'https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js',
							'https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js',
							'https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js',
							'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js',
							'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js',
							'https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js',
							'https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js '	
													))));			
	}

}