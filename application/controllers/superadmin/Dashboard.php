<?php 
class Dashboard  extends CI_Controller
{
	public function index()
	{
			$this->load->view('superadmin/html/head');			
			$this->load->view('superadmin/html/header');		
			$this->load->view('superadmin/html/nav');			
			$this->load->view('superadmin/dashboard');			
			$this->load->view('superadmin/html/footer');	
	}
}