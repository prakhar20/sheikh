<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('events_model','events');
		$events = $this->events->getCollection();
		foreach($events->result() as $event)
		{
			$response[] =  array(
							'title'=> $event->event_title,
							'start'=> $event->date,
							'imageurl'=> base_url($event->image),
						);

		}

		$this->load->view('index',array('collection'=>$events,'calender_events'=>$response));
	}
	public function rsvp()
	{
		$postData= $this->input->post();
		$resposne = $this->db->insert('rsvp_response',$postData);
		echo $resposne;
	}
}
