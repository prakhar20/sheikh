<?php 
class Auth_model extends CI_Model
{
	public function __construct()
	{
		$this->inputPassword = '';
		// parent::__construct();
	}

	public function getUserByEmail($email)
	{
		$this->db->where('email',$email);
		$result = $this->db->get('members')->row();
		return $result;
	}

	public function authenticate($email,$password)
	{
		$this->inputPassword = $password;
		$user = $this->getUserByEmail($email);
		// $this->verifyUser($user->password);
		if($this->verifyUser($user->password) === true)
			return $user;
		else
			return array();

	}
	private function verifyUser($password)
	{
		$this->load->library('bcrypt');
		return $this->bcrypt->check_password($this->inputPassword,$password);	
	}	
}
