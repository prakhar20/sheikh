<?php 
class Events_model extends CI_Model
{
	public function create($data)
	{	

		if (isset($data['id'])) 
		{
			$this->db->where('id',$data['id']);
		$query= 	$this->db->update('events',$data);
		}
		else
		{
		 $query = $this->db->insert('events',$data);
			
		}
		return $query;
	}


	public function getCollection()
	{
		return $this->db->get('events');
	}	
}

