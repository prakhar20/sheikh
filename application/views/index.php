<!doctype html>
<html class="no-js" lang="en">

<!-- Mirrored from themearth.com/demo/html/emeet/view/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 08 Aug 2018 08:13:44 GMT -->
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <!-- The above 3 meta tags *must* come first in the head -->

    <!-- SITE TITLE -->
    <title>The Visionary Leader</title>
    <meta name="description" content="Responsive EventHunt HTML Template"/>
    <meta name="keywords" content="Bootstrap3, Event,  Conference, Meetup, Template, Responsive, HTML5"/>
    <meta name="author" content="themearth.com"/>

    <!-- twitter card starts from here, if you don't need remove this section -->
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:site" content="@yourtwitterusername"/>
    <meta name="twitter:creator" content="@yourtwitterusername"/>
    <meta name="twitter:url" content="http://yourdomain.com/"/>
    <meta name="twitter:title" content="Your home page title, max 140 char"/>
    <!-- maximum 140 char -->
    <meta name="twitter:description" content="Your site description, maximum 140 char "/>
    <!-- maximum 140 char -->
    <meta name="twitter:image" content="assets/img/twittercardimg/twittercard-280-150.jpg"/>
    <!-- when you post this page url in twitter , this image will be shown -->
    <!-- twitter card ends from here -->

    <!-- facebook open graph starts from here, if you don't need then delete open graph related  -->
    <meta property="og:title" content="Your home page title"/>
    <meta property="og:url" content="http://your domain here.com"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:site_name" content="Your site name here"/>
    <!--meta property="fb:admins" content="" /-->  <!-- use this if you have  -->
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="assets/img/opengraph/fbphoto.jpg"/>
    <!-- when you post this page url in facebook , this image will be shown -->
    <!-- facebook open graph ends from here -->

    <!--  FAVICON AND TOUCH ICONS -->
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url('assets/img/favicon.ico')?>"/>
    <!-- this icon shows in browser toolbar -->
    <link rel="icon" type="image/x-icon" href="<?= base_url('assets/img/favicon.ico')?>"/>
    <!-- this icon shows in browser toolbar -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?= base_url('assets/img/favicon/apple-icon-57x57.png')?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= base_url('assets/img/favicon/apple-icon-60x60.png')?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url('assets/img/favicon/apple-icon-72x72.png')?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url('assets/img/favicon/apple-icon-76x76.png')?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url('assets/img/favicon/apple-icon-114x114.png') ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= base_url('assets/img/favicon/apple-icon-120x120.png')?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= base_url('assets/img/favicon/apple-icon-144x144.png')?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= base_url('assets/img/favicon/apple-icon-152x152.png')?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url('assets/img/favicon/apple-icon-180x180.png')?>">
    <link rel="icon" type="image/png" sizes="192x192" href="<?= base_url('assets/img/favicon/android-icon-192x192.png')?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url('assets/img/favicon/favicon-32x32.png')?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= base_url('assets/img/favicon/favicon-96x96.png')?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('assets/img/favicon/favicon-16x16.png')?>">
    <link rel="manifest" href="<?= base_url('assets/img/favicon/manifest.json')?>">

    <!-- BOOTSTRAP CSS -->
    <link rel="stylesheet" href="<?= base_url('assets/libs/bootstrap/css/bootstrap.min.css')?>" media="all"/>

    <!-- FONT AWESOME -->
    <link rel="stylesheet" href="<?= base_url('assets/libs/fontawesome/css/font-awesome.min.css')?>" media="all"/>  
    <!-- FONT AWESOME -->
    <link rel="stylesheet" href="<?= base_url('assets/libs/maginificpopup/magnific-popup.css')?>" media="all"/>
    <!-- Time Circle -->
    <link rel="stylesheet" href="<?= base_url('assets/libs/timer/TimeCircles.css')?>" media="all"/>
    <!-- OWL CAROUSEL CSS -->
    <link rel="stylesheet" href="<?= base_url('assets/libs/owlcarousel/owl.carousel.min.css')?>" media="all" />
    <link rel="stylesheet" href="<?= base_url('assets/libs/owlcarousel/owl.theme.default.min.css')?>" media="all" />

    <!-- GOOGLE FONT -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Oswald:400,700%7cPoppins:300,400,400i,600,600i,700,800,900"/>

    <!-- MASTER  STYLESHEET  -->
    <link id="lgx-master-style" rel="stylesheet" href="<?= base_url('assets/css/style-default.min.css')?>" media="all"/>

    <!-- custom css -->
    <link id="lgx-master-style" rel="stylesheet" href="<?= base_url('assets/css/custom.css')?>" media="all"/>  
      <link rel="stylesheet" href="<?= base_url('assets/bower_components/fullcalendar/dist/fullcalendar.min.css')?>">
  <link rel="stylesheet" href="<?= base_url('assets/bower_components/fullcalendar/dist/fullcalendar.print.min.css')?>" media="print">
    <!-- adding font family -->

    <link rel="stylesheet" type="text/css" href="assets/font-family/">

  <link rel="stylesheet" href="<?= base_url('assets/plugins/iCheck/all.css')?>">
    <!-- MODERNIZER CSS  -->
    <script src="<?= base_url('assets/js/vendor/modernizr-2.8.3.min.js')?>"></script>
    <style>
    input[type='number'] {
    -moz-appearance:textfield;
    }
    input[type=number]::-webkit-inner-spin-button, 
    input[type=number]::-webkit-outer-spin-button { 
    -webkit-appearance: none; 
        margin: 0; 
    }

    
    th.ui-widget-header {
    font-size: 9pt;
/*    font-family: Verdana, Arial, Sans-Serif;*/
}
    </style>
</head>

<body class="home">

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<div class="lgx-container ">
<!-- ***  ADD YOUR SITE CONTENT HERE *** -->


<!--HEADER-->
<header>
    <div id="lgx-header" class="lgx-header">
        <div class="lgx-header-position lgx-header-position-white lgx-header-position-fixed "> <!--lgx-header-position-fixed lgx-header-position-white lgx-header-fixed-container lgx-header-fixed-container-gap lgx-header-position-white-->
            <div class="lgx-container"> <!--lgx-container-fluid-->
                <nav class="navbar navbar-default lgx-navbar">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="lgx-logo">
                            <a href="index.html" class="lgx-scroll lgx-scroll-home-logo">
                                <img src="assets/img/images/logo.png" alt="Eventhunt Logo"/>
                            </a>
                        </div>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse lgx-collapse">
                        <ul class="nav navbar-nav lgx-nav lgx-nav-full">
                            <li>
                                <a href="#lgx-home" class="dropdown-toggle active lgx-scroll">Home </a>
                            <li>
                            <li><a class="lgx-scroll" href="#lgx-about">About</a></li>
                            <li><a class="lgx-scroll" href="#lgx-historic-attempt">Historic attempt</a></li>
                            <li><a class="lgx-scroll" href="#lgx-schedule">Event Schedule</a></li>
                            <li><a class="lgx-scroll" href="#lgx-event-info">Event Information</a></li>
                        </ul>
                        <div class="lgx-nav-right lgx-nav-right-custom">
                            <div class="lgx-cart-area">
                                <a href="index.html" class="lgx-scroll lgx-scroll-image-right">
                                <img src="assets/img/images/year_of_zayed_logo.png" alt="Eventhunt Logo"/>
                            </a>
                            </div>
                        </div>
                    </div><!--/.nav-collapse -->
                </nav>
            </div>
            <!-- //.CONTAINER -->
        </div>
    </div>
</header>
<!--HEADER END-->


<!--BANNER-->
<section>
    <div id="lgx-home" class="lgx-banner">
        <div class="lgx-banner-style">
            <div class="lgx-inner lgx-inner-fixed lgx-inner-fixed-custom">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="lgx-banner-info-area">
                                <div class="lgx-banner-info-circle">
                                    <div class="info-circle-inner">
                                        <h3 class="date"><b class="lgx-counter">29</b> <span>November</span></h3>
                                        <div class="lgx-countdown-area">
                                            <!-- Date Format :"Y/m/d" || For Example: 1017/10/5  -->
                                            <div id="lgx-countdown" data-date="2019/12/15"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="lgx-banner-info lgx-banner-info-custom"> <!--lgx-banner-info-center lgx-banner-info-black lgx-banner-info-big lgx-banner-info-bg--> <!--banner-info-margin-->
                                    <!-- <h3 class="subtitle">Learn Anything</h3>
                                    <h2 class="title">Conference Meet <span><b>2</b><b>0</b><b>1</b><b>9</b></span></h2>
                                    <h3 class="location"><i class="fa fa-map-marker"></i> 21 King Street, Dhaka, Bangladesh.</h3> -->

                                    <h1 class="remembering-header">REMEMBERING SHEIKH ZAYED ON HIS 100TH BIRTHDAY,</h1>
                                    <div class="quotes-sub-header">
	                                    <P>THE MAN WHO BUILT THE UAE.</P>
	                                    <P>THE SYMBOL OF JUSTICE</P>
	                                    <P>GENEROSITY AND COURAGE</P>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//.ROW-->
                </div>
                <!-- //.CONTAINER -->
            </div>
            <!-- //.INNER -->
        </div>
    </div>
</section>
<!--BANNER END-->


<!--ABOUT-->
<section>
    <div id="lgx-about" class="lgx-about lgx-about-image">
        <div class="lgx-inner">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-7">
                        <div class="lgx-about-content-area">
                            <div class="lgx-heading">
                                <h2 class="heading heading-event">ABOUT THE EVENT</h2>
                                <h3 class="subheading subheading-event">107 + EVENTS</h3>
                            </div>
                            <div class="lgx-about-content">
                                <p class="text text-modified">
                                    We bring to you a perfect blend of cultural
                                    variety and excellent display of artistic skills
                                    through this event.        
                                </p>
                                <div class="about-date-area about-date-area-custom">
                                    <h4 class="date"><span>20</span></h4>
                                    <p class="event-date"><span class="date-modify">EVENT START ON <br> AUGUST</span> 
                                        <span class="description">Great ceremony the portrayal <br> on 02 December 2018.
                                        </span>
                                    </p>
                                </div>

                                <div class="section-btn-area">
                                    <a class="lgx-btn" href="about.html"><span class="font-wight-blod">READ MORE</span></a>
                                    <a class="lgx-btn lgx-btn-red lgx-scroll lgx-scroll-about-event" href="#lgx-registration"><span class="font-wight-blod">SHOW YOUR INTEREST</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-5 event-schedule-image-width-pad">
                        <div class="lgx-about-img-sp lgx-about-img-sp-custom">
                            <img src="assets/img/images/about-sp.jpg" alt="about">
                        </div>
                    </div>
                </div>
            </div><!-- //.CONTAINER -->
        </div><!-- //.INNER -->
    </div>
</section>
<!--ABOUT END-->


<!--SPEAKERS-->
<section>
    <div id="lgx-historic-attempt" class="lgx-speakers lgx-speakers2">
        <div class="container-fluid historic-attempt">
        <div class="row">
            <div class="col-sm-12 col-md-5 padding-left-zero">
                <div class="lgx-about-img-sp lgx-about-img-sp-custom">
                    <img src="assets/img/images/painting.jpg" alt="about">
                </div>
            </div>
            <div class="col-sm-12 col-md-5 historic-attempt-right-block">
            <h2 class="historic-attempt-never">HISTORIC ATTEMPT <br> NEVER BEFORE</h2>
            <p class="world-record-attempt">World Record Attempt for Portrait Painting of
                His Highness Sheikh Zayed bin Sultan Al Nahyan
                The Largest Professional Oil Painting
                by a Single Artist <br>
                Size 100Ft X 60Ft | Oil Painting | Media: Canvas
                3 Months LIVE Painting by single Artist
                Inaugurating the Portrait on 02 December 2018.</p>
                <div class="about-date-area about-date-area-custom">
                    <h4 class="date date-modification-dec"><span>02</span></h4>
                    <p class="event-date event-date-custom"><span class="date-modify">DECEMBER <br> 2018</span> 
                    </p>
                </div>
                 <div class="section-btn-area section-btn-area-custom">
                        <a class="lgx-btn" href="#" data-toggle="modal" data-target="#modal-default" ><span class="font-wight-blod">SHOW YOUR INTEREST</span></a>
                    </div>
            </div>
        </div>
            <!-- //.CONTAINER -->
        </div>
        <!-- //.INNER -->
    </div>
</section>
<!--SPEAKERS END-->





<!--SCHEDULE-->
<section>
    <div id="lgx-schedule" class="lgx-schedule lgx-schedule-mobile lgx-inner-slider">
        <div class="lgx-inner lgx-inner-slider">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="lgx-heading lgx-heading-custom lgx-heading-white">
                            <h2 class="heading heading-event-schedule">Event Schedule</h2>
                        </div>
                    </div>
                    </div>
                    <div class="row">
                    <div id="lgx-owltestimonial" class="lgx-owltestimonial lgx-owlnews">

                    <?php foreach($collection->result() as $event):?>
                    <div class="item">
                        <blockquote class="lgx-testi-single">
                        <h3 class="event-main-hrader"><?php printf('%03d',$event->id);?></h3>
                        <p class="date-mentioned-class"><?= strtoupper(str_replace(',',' ',date("jS,F, Y", strtotime("2011-01-05"))));?></p>
                        <p class="the-day-of-purity"><?= strtoupper($event->event_title)?></p>
                        <p class="statement"><?= $event->event_description ?></p>
                        </blockquote>
                    </div>
                <?php endforeach; ?>
                    <div class="item">
                    <blockquote class="lgx-testi-single">
                        <h3 class="event-main-hrader">EVENT 002</h3>
                        <p class="date-mentioned-class">20TH AUGUST 2018</p>
                        <p class="the-day-of-purity">THE DAY OF PURITY</p>
                        <p class="statement">Purity is the absence of impurity. It is related to
                            guiltless, blameless or innocent behaviour. <br>
                            “Purity is the gate keeper for everything precious
                            and blissful in Gods kingdom”</p>
                            </blockquote>
                    </div>
                    </div>
                    </div>
                </div>
            </div>
            <!-- //.CONTAINER -->
        </div>
        <!-- //.INNER -->
    </div>
</section>
<!--SCHEDULE END-->

  <!--TRAVEL INFO-->
    <section>
        <div id="lgx-event-info" class="lgx-travelinfo lgx-inner-event-bg">
            <div class="lgx-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="lgx-heading lgx-heading-Event-Information">
                                <h2 class="heading heading-Event-Information">Event Information</h2>
                            </div>
                        </div>
                        <!--//main COL-->
                    </div>
                    <div class="row">
                        <div class="col-xs-12 event-schedule-padding">
                            <div class="lgx-travelinfo-content">
                                <div class="lgx-travelinfo-single">
                                <div class="event-schedule-image">
                                    <img src="assets/img/images/calender.png" alt="location"/>
                                 </div>   
                                    <h3 class="title">EVENT SCHEDULE</h3>
                                    <p class="info">
                                        MORNING :10AM - 12PM <br>
                                        MID TIME :03PM - 06PM <br>
                                        PRIME :06PM - 10PM 
                                    </p>
                                </div>
                                <div class="lgx-travelinfo-single">
                                   <div class="venue-image">
                                        <img src="assets/img/images/venue.png" alt="Transport"/>
                                    </div>
                                    <h3 class="title">VENUE</h3>
                                    <p class="info">DUBAI, UAE</p>
                                </div>
                                <div class="lgx-travelinfo-single">
                                    <div class="event-timing-image">
                                        <img src="assets/img/images/clock.png" alt="Hotel & Restaurant"/>
                                    </div>
                                    <h3 class="title">EVENT TIMING</h3>
                                    <p class="info">SATURDAY TO THURSDAY <br>
                                            10AM TO 10PM <br>
                                            FRIDAY <br>
                                            4PM TO 11PM</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                          <div class="section-btn-area section-btn-area-custom-event">
                        <a class="lgx-btn" href="about.html"><span class="font-wight-blod">SHOW YOUR INTEREST</span></a>
                    </div>
                    </div>
                    <!--//.ROW-->
                </div>
                <!-- //.CONTAINER -->
            </div>
        </div>
    </section>
    <!--TRAVEL INFO END-->
    <!-- calender section start -->
    <section>
        <div id="lgx-event-calender" class=""> 
            <div class="lgx-inner">
                <div class="container">
                     <div class="row">
                        <div class="col-xs-12">
                            <div class="lgx-heading lgx-heading-custom lgx-heading-white">
                                <h2 class="heading heading-event-schedule">EVENT CALENDAR</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                        <div class="event-calender" id="events_calendar">
                            
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--calender section ends -->
    <!--REGISTRATION-->
    <section>
        <div id="lgx-registration" class="lgx-registration"> <!--lgx-registration2 lgx-registration3 lgx-registration4-->
            <div class="lgx-inner lgx-inner-participent">
                <div class="container">
                     <div class="row">
                        <div class="col-xs-12">
                            <div class="lgx-heading lgx-heading-custom lgx-heading-white">
                                <h2 class="heading heading-event-schedule">PARTICIPATIONS</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                        <div class="participants-items"></div>
                        </div>
                    </div>
                </div><!-- //.CONTAINER -->
            </div><!-- //.INNER -->
        </div>
    </section>
    <!--REGISTRATION END-->

 <!--Participation Form  INFO-->
    <section>
        <div id="lgx-event-info" class="lgx-travelinfo lgx-inner-event-bg">
            <div class="lgx-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="lgx-heading lgx-heading-Event-Information">
                                <h2 class="heading heading-Event-Information">Event Participation</h2>
                            </div>
                        </div>
                        <!--//main COL-->
                    </div>
                    <div class="row">
                        <form action="upload_video" method="post" enctype="multipart/form-data">
                        <div class="col-xs-12 event-schedule-padding">
                            <div class="lgx-travelinfo-content">
                                <div class="form-group">
                                    <label>
                                      <input type="radio" required name="interest" value="sponsor" class="flat-red" checked>Sponsor
                                    </label>
                                    <label>
                                      <input type="radio" required name="interest" value="participant" class="flat-red">Participant
                                    </label>
                                    <label>
                                      <input type="radio" required name="interest" value="info" class="flat-red">Get Event Info
                                    </label>
                                </div> 
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Name</label>
                                  <input type="text" name="name" class="form-control" id="exampleInputEmail1" placeholder="Enter Your Name" value="" required >
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Contact</label>
                                  <input type="number" name="contact" class="form-control" id="exampleInputEmail1" placeholder="Enter Your Mobile Number" pattern="\d*" maxlength="10" value="" required >
                                </div>
                                 <div class="form-group">
                                  <label for="exampleInputEmail1">Email</label>
                                  <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Enter Your Email" value=""  required>
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Upload Video</label>
                                  <input type="file" name="video" class="form-control" id="exampleInputEmail1" placeholder="" value=""  required>
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Upload Video</label>
                                  <input type="submit" name="submit" class="form-control" id="exampleInputEmail1" placeholder="" value=""  required>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                    </div>
                    <!--//.ROW-->
                </div>
                <!-- //.CONTAINER -->
            </div>
        </div>
    </section>
    <!--TRAVEL INFO END-->

<!--SPONSORED-->
<section>
    <div id="lgx-sponsors" class="lgx-sponsors">
        <div class="lgx-inner-bg">
            <div class="lgx-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="lgx-heading lgx-heading-custom lgx-heading-white">
                                <h2 class="heading heading-event-schedule official-spons-btm">OFFCIAL SPONSONRS</h2>
                            </div>
                        </div>
                    </div>
                    <!--//main row-->
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="sponsors-area sponsors-area-nogap sponsors-area-noshadow sponsors-area-col3"> <!--sponsors-area-col3 sponsors-area-noshadow sponsors-area-nogap sponsors-area-colorfull sponsors-area-colorfull-border sponsors-area-border-->
                                <div class="single">
                                    <a class="" href="#"><img src="assets/img/sponsors/sponsor-sp3.png" alt="sponsor"/></a>
                                </div>
                                <div class="single">
                                    <a class="" href="#"><img src="assets/img/sponsors/sponsor-sp2.png" alt="sponsor"/></a>
                                </div>
                                <div class="single">
                                    <a class="" href="#"><img src="assets/img/sponsors/sponsor3.png" alt="sponsor"/></a>
                                </div>
                            </div>
                        </div>
                        <!--//col-->
                    </div>
                    <!--//row-->
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="sponsors-area sponsors-area-noshadow sponsors-area-nogap">
                                <div class="single">
                                    <a class="" href="#"><img src="assets/img/sponsors/sponsor4.png" alt="sponsor"/></a>
                                </div>
                                <div class="single">
                                    <a class="" href="#"><img src="assets/img/sponsors/sponsor-sp1.png" alt="sponsor"/></a>
                                </div>
                                <div class="single">
                                    <a class="" href="#"><img src="assets/img/sponsors/sponsor3.png" alt="sponsor"/></a>
                                </div>
                            </div>
                        </div>
                        <!--//col-->
                    </div>
                </div>
                <!--//container-->
            </div>
        </div>
        <!--//lgx-inner-->
    </div>
</section>
<!--SPONSORED END-->


    <!--TESTIMONIALS -->
 
    <!--TESTIMONIALS END-->


    <!--News-->
   
    <!--News END-->



<!--VIDEO-->

<!--//.VIDEO END-->



        <div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Show Your Interests</h4>
              </div>
              <div class="modal-body">
                <form action="#" id="rsvp_form">
                <div class="form-group">
                    <label>
                      <input type="radio" required name="interest" value="sponsor" class="flat-red" checked>Sponsor
                    </label>
                    <label>
                      <input type="radio" required name="interest" value="participant" class="flat-red">Participant
                    </label>
                    <label>
                      <input type="radio" required name="interest" value="info" class="flat-red">Get Event Info
                    </label>
                </div> 
                <div class="form-group">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="text" name="name" class="form-control" id="exampleInputEmail1" placeholder="Enter Your Name" value="" required >
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Contact</label>
                  <input type="number" name="contact" class="form-control" id="exampleInputEmail1" placeholder="Enter Your Mobile Number" pattern="\d*" maxlength="10" value="" required >
                </div>
                 <div class="form-group">
                  <label for="exampleInputEmail1">Email</label>
                  <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Enter Your Email" value=""  required>
                </div>
            </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" id="modal_rsvp" class="btn btn-primary">Save changes</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>


<!--FOOTER-->
<footer>
    <div id="lgx-footer" class="lgx-footer"> <!--lgx-footer-white-->
        <div class="lgx-inner-footer footer-bg-image">
            <div class="lgx-subscriber-area ">
                <div class="container">
                    <div class="lgx-subscriber-inner">  <!--lgx-subscriber-inner-indiv-->
                        <div class="lgx-heading lgx-heading-custom lgx-heading-white">
                                <h2 class="heading heading-event-schedule official-spons-btm">JOIN NEWSLETTER</h2>
                            </div>
                        <form class="lgx-subscribe-form" >
                            <div class="form-group form-group-email">
                                <input type="email" id="subscribe" placeholder="Enter your email Address  ..." class="form-control form-control-custom lgx-input-form form-control"  />
                            </div>
                            <div class="form-group form-group-submit form-group-submit-custom">
                                <button type="submit" name="lgx-submit" id="lgx-submit" class="lgx-btn lgx-submit"><span>Subscribe</span></button>
                            </div>
                        </form> <!--//.SUBSCRIBE-->
                    </div>
                </div>
            </div>
            <div class="container" style="">
                <div class="lgx-footer-area lgx-footer-area-custom">
                    <div class="lgx-footer-single">
                        <a class="logo" href="index.html"><img src="assets/img/images/logo_end.png" alt="Logo"></a>
                    </div> <!--//footer-area-->
                    <div class="lgx-footer-single">
                        <h3 class="footer-title">Venue Location </h3>
                        <h4 class="date">
                            20 AUG - 2 December, 2019
                        </h4>
                        <address>
                            85 Golden Street, Darlinghurst <br>
                            ERP 2019, United States
                        </address>
                        <a id="myModalLabel2" data-toggle="modal" data-target="#lgx-modal-map" class="map-link" href="#"><i class="fa fa-map-marker" aria-hidden="true"></i> View Map location</a>
                    </div>
                    <div class="lgx-footer-single">
                        <h3 class="footer-title">Social Connection</h3>
                        <p class="text">
                            You should connect social area <br> for Any update
                        </p>
                        <ul class="list-inline lgx-social-footer">
                            <li><a href="#" class="footer-bg"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="footer-bg"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="footer-bg"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                    <!--<div class="lgx-footer-single">
                        <h2 class="footer-title">Instagram Feed</h2>
                        <div id="instafeed">
                        </div>
                    </div>-->
                </div>
                <!-- Modal-->
                <div id="lgx-modal-map" class="modal fade lgx-modal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <div class="lgxmapcanvas map-canvas-default" id="map_canvas"> </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- //.Modal-->

                <div class="lgx-footer-bottom">
                    <div class="lgx-copyright">
                        <p> <span>©</span> 2018 All rights reserved</p>
                    </div>
                </div>

            </div>
            <!-- //.CONTAINER -->
        </div>
        <!-- //.footer Middle -->
    </div>
</footer>
<!--FOOTER END-->


</div>
<!--//.LGX SITE CONTAINER-->
<!-- *** ADD YOUR SITE SCRIPT HERE *** -->
<!-- JQUERY  -->
<script src="<?= base_url('assets/js/vendor/jquery-1.12.4.min.js')?>"></script>

<!-- BOOTSTRAP JS  -->
<script src="<?= base_url('assets/libs/bootstrap/js/bootstrap.min.js')?>"></script>

<!-- Smooth Scroll  -->
<script src="<?= base_url('assets/libs/jquery.smooth-scroll.js')?>"></script>

<!-- SKILLS SCRIPT  -->
<script src="<?= base_url('assets/libs/jquery.validate.js')?>"></script>

<!-- if load google maps then load this api, change api key as it may expire for limit cross as this is provided with any theme -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQvRGGtL6OrpP5xVMxq_0NgiMiRhm3ycI"></script>

<!-- CUSTOM GOOGLE MAP -->
<script type="text/javascript" src="<?= base_url('assets/libs/gmap/jquery.googlemap.js')?>"></script>

<!-- adding magnific popup js library -->
<script type="text/javascript" src="<?= base_url('assets/libs/maginificpopup/jquery.magnific-popup.min.js')?>"></script>

<!-- Owl Carousel  -->
<script src="<?= base_url('assets/libs/owlcarousel/owl.carousel.min.js')?>"></script>

<!-- COUNTDOWN   -->
<script src="<?= base_url('assets/libs/countdown.js')?>"></script>
<script src="<?= base_url('assets/libs/timer/TimeCircles.js')?>"></script>

<!-- Counter JS -->
<script src="<?= base_url('assets/libs/waypoints.min.js')?>"></script>
<script src="<?= base_url('assets/libs/counterup/jquery.counterup.min.js')?>"></script>

<!-- SMOTH SCROLL -->
<script src="<?= base_url('assets/libs/jquery.smooth-scroll.min.js')?>"></script>
<script src="<?= base_url('assets/libs/jquery.easing.min.js')?>"></script>

<!-- type js -->
<script src="<?= base_url('assets/libs/typed/typed.min.js')?>"></script>

<!-- header parallax js -->
<script src="<?= base_url('assets/libs/header-parallax.js')?>"></script>

<!-- instafeed js -->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/instafeed.js/1.4.1/instafeed.min.js"></script>-->
<script src="<?= base_url('assets/libs/instafeed.min.js')?>"></script>
<script src="<?= base_url('assets/bower_components/moment/moment.js')?>"></script>
<script src="<?= base_url('assets/bower_components/fullcalendar/dist/fullcalendar.min.js')?>"></script>
<!-- CUSTOM SCRIPT  -->
<script src="<?= base_url('assets/js/custom.script.js')?>"></script>

<!-- <div class="lgx-switcher-loader"></div> -->
<!-- For Demo Purpose Only// Remove From Live -->
<script src="<?= base_url('switcher/js/switcherd41d.js')?>"></script>
<!-- For Demo Purpose Only //Remove From Live-->

</script>
<script src="https://unpkg.com/sweetalert2@7.17.0/dist/sweetalert2.all.js"></script>
<script src="<?= base_url('assets/plugins/iCheck/icheck.min.js')?>"></script>"
<script>
   $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

   $('#modal_rsvp').click(function(){
    var form  =$('#rsvp_form')[0];
    if(form.checkValidity())
    {
     var datastr = $('#rsvp_form').serialize();
      $.ajax({url: "<?= base_url('rsvp')?>",type:"POST",data:datastr, success: function(result){
        if(result)
        {
            form.reset();
            swal("Thank You !", "For Your Response!", "success");
                    $("#modal-default").modal('hide');
        }
        }}); 
        
    }
    else
    {
        form.reportValidity();
        form.preventDefault();
    }
   });
</script>
<script type="text/javascript">
        $(function () {

    /* initialize the external events
     -----------------------------------------------------------------*/
    function init_events(ele) {
      ele.each(function () {

        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
        // it doesn't need to have a start or end
        var eventObject = {
          // use the element's text as the event title
          title: $.trim($(this).text()), 
          imageurl : "assets/img/schedule/speaker1.jpg",
          start  : '2018-08-01'
        }

        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject)

        // make the event draggable using jQuery UI
        $(this).draggable({
          zIndex        : 1070,
          revert        : true, // will cause the event to go back to its
          revertDuration: 0  //  original position after the drag
        })

      })
    }

    init_events($('#external-events div.external-event'))

    /* initialize the calendar
     -----------------------------------------------------------------*/
    //Date for the calendar events (dummy data)
    var date = new Date()
    var d    = date.getDate(),
        m    = date.getMonth(),
        y    = date.getFullYear()

    $('#events_calendar').fullCalendar({
      header    : {
        left  : 'prev,next today',
        center: 'title',
        right : 'month,agendaWeek,agendaDay'
      },
      buttonText: {
        today: 'today',
        month: 'month',
        week : 'week',
        day  : 'day'
      },
        height: 450,
        width:500,
      //Random default events
      //
      eventColor: 'rgba(150,150,150, 0.5)',
      events    : <?= json_encode($calender_events)?>,


      /*eventConstraint:{
          start: '00:00', // a start time (start of the day in this example)
          end: '24:00', // an end time (end of the day in this example)
      }, */

      eventRender: function(event, eventElement,view) {
          // event.backgroundColor = 'cccccc#';
          
          if (event.imageurl) {
              var dateString = event.start.format("YYYY-MM-DD");
              img = event.imageurl;
              
              $(view.el[0]).find('.fc-day[data-date=' + dateString + ']').css('background-image', 'url("'+img+'")').css('background-size','cover').css('background-repeat','no-repeat').css('background-position','center center');
              // $(view.el[0]).find('.fc-day[data-date=' + dateString + ']').css('background-image', 'url("'+BASE_URL+event.imageurl'")');
              // eventElement.find("div.fc-content").prepend("<img src='" + BASE_URL+event.imageurl +"' width='12' height='12'>");
          }
      },
      eventDrop: function(event, delta, revertFunc) {

        if (!event.start.startOf('day').isSame(event.end.startOf('day'))) {
             revertFunc();
        }

      },  


      editable  : false,
      droppable : false, // this allows things to be dropped onto the calendar !!!
      drop      : function (date, allDay) { // this function is called when something is dropped

        // retrieve the dropped element's stored Event Object
        var originalEventObject = $(this).data('eventObject')

        // we need to copy it, so that multiple events don't have a reference to the same object
        var copiedEventObject = $.extend({}, originalEventObject)

        // assign it the date that was reported
        copiedEventObject.start           = date
        copiedEventObject.allDay          = allDay
        copiedEventObject.backgroundColor = $(this).css('background-color')
        copiedEventObject.borderColor     = $(this).css('border-color')

        // render the event on the calendar
        // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
        $('#calendar').fullCalendar('renderEvent', copiedEventObject, true)

        // is the "remove after drop" checkbox checked?
        if ($('#drop-remove').is(':checked')) {
          // if so, remove the element from the "Draggable Events" list
          $(this).remove()
        }

      }
    })

    /* ADDING EVENTS */
    var currColor = '#3c8dbc' //Red by default
    //Color chooser button
    var colorChooser = $('#color-chooser-btn')
    $('#color-chooser > li > a').click(function (e) {
      e.preventDefault()
      //Save color
      currColor = $(this).css('color')
      //Add color effect to button
      $('#add-new-event').css({ 'background-color': currColor, 'border-color': currColor })
    })
    $('#add-new-event').click(function (e) {
      e.preventDefault()
      //Get value and make sure it is not null
      var val = $('#new-event').val()
      if (val.length == 0) {
        return
      }

      //Create events
      var event = $('<div />')
      event.css({
        'background-color': currColor,
        'border-color'    : currColor,
        'color'           : '#fff'
      }).addClass('external-event')
      event.html(val)
      $('#external-events').prepend(event)

      //Add draggable funtionality
      init_events(event)

      //Remove event from text input
      $('#new-event').val('')
    })
  })

      </script>
</body>

</html>
