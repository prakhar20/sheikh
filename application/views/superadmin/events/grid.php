    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Events List
        <small>Listing All Events</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Events</a></li>
        <li class="active">List</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Events</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="datatable_events" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Event Title</th>
                  <th>Event Performer</th>
                  <th>Date</th>
                  <th>Image</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                foreach($collection->result() as $row ) : 
                ?>
                <tr>
                  <td><?= $row->id?></td>
                  <td><?= $row->event_title?></td>
                  <td><?= $row->event_performer ?></td>
                  <td><?= $row->date ?></td>
                  <td><img src= "<?= base_url($row->image) ?>" height="100" width="100"></td>
                  <td><a href="<?= base_url('superadmin/events/edit/'.$row->id)?>" class="btn btn-warning">Edit</a>
                    
                    <a href="<?= base_url('superadmin/events/delete/'.$row->id)?>" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this event?');">Delete</a>
                  </td>
                </tr>
                <?php endforeach;?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
  </div>
<script>
  $(function () {
    $('#datatable_events').DataTable(
    {
      dom: 'Bfrtip',
      buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
    })
  })
</script>