
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        General Form Elements
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">General Elements</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-8">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Quick Example</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="<?= base_url('superadmin/events/save')?>" method="post" role="form" enctype="multipart/form-data">
              
            <?php if(isset($edit) ):?>
              <input type="hidden" name="id" value="<?= $collection->row()->id ?>"/>
            <?php endif;?>
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputPassword1">Event Title</label>
                  <input type="text" name="event_title" class="form-control" id="exampleInputPassword1" placeholder="Event Title" value=<?= (isset($edit) ? $collection->row()->event_title : '')?> >
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Preformer Name</label>
                  <input type="text" name="event_performer" class="form-control" id="exampleInputEmail1" placeholder="Enter Performer Name" value="<?= (isset($edit) ? $collection->row()->event_performer : '')?>" >
                </div>


                <div class="form-group">
                  <label for="exampleInputFile">Calender Image</label>

                    <?php if(isset($edit)):?>
                    </br>
                      <a href="<?= base_url($collection->row()->image) ?>" target="_blank">
                          <img src= "<?= base_url($collection->row()->image) ?>" height="100" width="100">
                      </a>
                    <?php endif;?>
                  <input type="file" id="exampleInputFile" name="image" >

                  <p class="help-block">This Image will be shown on the calender backgroud , its prefferd it should be 150px X 150px Or A  Square Image.</p>
                </div>
                <div class="form-group">
                  <label>Event Description</label>
                  <textarea class="form-control" id="event_description" name="event_description" rows="3" placeholder="Enter Event Description ..."><?= (isset($edit) ? $collection->row()->event_description : '')?></textarea>
                </div>
                <div class="form-group">
                    <label>Date:</label>
                      <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>

                      <input value="<?= (isset($edit) ? $collection->row()->date : '')?>" name="date" class="form-control pull-right" id="datepicker" type="text">
                      </div>
                <!-- /.input group -->
               </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
  </div>
  <script>
    $(document).ready(function () {
      $('#datepicker').datepicker().datepicker("setDate", new Date());
       CKEDITOR.replace('event_description');

    });
  </script>