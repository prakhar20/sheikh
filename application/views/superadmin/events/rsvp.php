    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        RSVP List
        <small>Listing All Responses</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">RSVP</a></li>
        <li class="active">List</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Response</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="datatable_events" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Interest</th>
                  <th>Name</th>
                  <th>Contact</th>
                  <th>Email</th>

                </tr>
                </thead>
                <tbody>
                <?php 
                foreach($collection->result() as $row ) : 
                ?>
                <tr>
                  <td><?= $row->interest?></td>
                  <td><?= $row->name?></td>
                  <td><?= $row->contact ?></td>
                  <td><?= $row->email ?></td>
                </tr>
                <?php endforeach;?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
  </div>
<script>
  $(function () {
    $('#datatable_events').DataTable(
    {
      dom: 'Bfrtip',
      buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
    })
  })
</script>